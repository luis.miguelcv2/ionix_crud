package com.ionix.crud.utils;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;


public class Utils {
  private final static String KEY = "ionix123456";

  public static String desCipher(String text) {
    String cipherTxt = null;
    try {
      DESKeySpec keySpec = new DESKeySpec(KEY.getBytes("UTF8"));
      SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
      SecretKey ks = keyFactory.generateSecret(keySpec);
      byte[] cleartext = text.getBytes("UTF8");
      Cipher cipher = Cipher.getInstance("DES");
      cipher.init(Cipher.ENCRYPT_MODE, ks);
      cipherTxt = Base64.getEncoder().encodeToString(cipher.doFinal(cleartext));
    } catch (Exception e) {
      e.printStackTrace();
    }
   return cipherTxt;
  } 

  public static String printJSON(Object objJSON)	{
		String jsonString = "";		
		ObjectMapper map = new ObjectMapper();
		map.setPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CAMEL_CASE);	  
	  
		try { 
			jsonString = map.writerWithDefaultPrettyPrinter().writeValueAsString(objJSON);			
		} 
		catch (JsonProcessingException e) { 
			e.printStackTrace(); 
		} 
		
		return jsonString;
	}
}
