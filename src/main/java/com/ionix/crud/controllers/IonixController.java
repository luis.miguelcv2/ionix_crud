package com.ionix.crud.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ionix.crud.dto.ResponseIonixDTO;
import com.ionix.crud.dto.ResponseIonixDTO.TotalItems;
import com.ionix.crud.dto.ResponseMockarooApiDTO;
import com.ionix.crud.services.IonixService;
import com.ionix.crud.utils.Utils;

@RestController
public class IonixController {

  @Autowired
  IonixService ionixService;

  @PostMapping(path = "/ionix/mockaroo", produces = "application/json")
  public ResponseEntity<Object> reportSalesOperations(HttpServletRequest request) throws JsonMappingException, JsonProcessingException {
    long startTime = System.nanoTime();
    HttpStatus httpStatus = HttpStatus.OK;
    HttpHeaders responseHeaders = new HttpHeaders();
    String jsonResp = "";

    String paramCipher = Utils.desCipher(request.getHeader("param"));
    List<Object> resp = ionixService.mockarooApi(paramCipher);
    int totalItems = 0;
    
    if(!resp.isEmpty()) {
      httpStatus = HttpStatus.valueOf((Integer) resp.get(0));
      String body = (String) resp.get(1);
      
      if(httpStatus.equals(HttpStatus.OK)) {
        ResponseMockarooApiDTO dto = new ObjectMapper().readValue(body, new TypeReference<ResponseMockarooApiDTO>(){});
        totalItems = dto.getResult().getItems().size();

        ResponseIonixDTO ionix = new ResponseIonixDTO();
        ionix.setResponseCode(dto.getResponseCode());
        ionix.setDescription(dto.getDescription());
        ionix.setElapsedTime(System.nanoTime() - startTime);
        TotalItems items = new TotalItems();
        items.setRegisterCount(totalItems);
        ionix.setResult(items);

        jsonResp = Utils.printJSON(ionix);
      } else {
        jsonResp = body;
      }
    } else {
      jsonResp = "Error Desconocido";
    }
    return new ResponseEntity<>(jsonResp, responseHeaders, httpStatus);
  }
  
}
