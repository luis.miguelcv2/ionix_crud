package com.ionix.crud.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ionix.crud.model.User;
import com.ionix.crud.services.UserService;

@Controller
public class UserController {
  
  @Autowired
  UserService userService;
  
  @RequestMapping("/")
  public String idex(Model model) {
     return showUserList(model);
  }

  @RequestMapping("/index")
  public String showUserList(Model model) {
      model.addAttribute("users", userService.findUserAll());
      return "index";
  }

  @RequestMapping("/user/find/{username}")
  public String findUserbyUsername(@PathVariable("username") String username, Model model) {
      model.addAttribute("users", userService.findAllByUsername(username));
      return "index";
  }
  
  @RequestMapping("/admin/signup")
  public String showSignUpForm(Model model) {
    model.addAttribute("user", new User());
    return "add-user";
  }
  
  @PostMapping("/admin/adduser")
  public String addUser(@Valid User user, BindingResult result, Model model, RedirectAttributes ra) {
    if (result.hasErrors()) {
      return "add-user";
    }
    
    userService.saveUser(user);
    ra.addFlashAttribute("message", "Usuario " + user.getName() + " añadido con exito!");
    return "redirect:/index";
  }
  
  @GetMapping("/admin/edit/{id}")
  public String showUpdateForm(@PathVariable("id") long id, Model model) {
    User user = userService.findUserById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
    model.addAttribute("user", user);
    
    return "update-user";
  }
  
  @PostMapping("/admin/update/{id}")
  public String updateUser(@PathVariable("id") long id, @Valid User user, BindingResult result, Model model, RedirectAttributes ra) {
    if (result.hasErrors()) {
        user.setId(id);
        return "update-user";
    }
    
    userService.saveUser(user);
    ra.addFlashAttribute("message", "Usuario " + user.getName() + " actualizado con exito!");
    return "redirect:/index";
  }
  
  @GetMapping("/admin/delete/{id}")
  public String deleteUser(@PathVariable("id") long id, Model model, RedirectAttributes ra) {
    User user = userService.findUserById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
    userService.deleteUser(user);
    
    ra.addFlashAttribute("message", "Usuario " + user.getName() + " eliminado con exito!");
    return "redirect:/index";
  }
}
