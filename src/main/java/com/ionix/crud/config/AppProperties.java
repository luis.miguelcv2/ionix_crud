package com.ionix.crud.config;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class AppProperties {
  public static String propertyFileName = "application";
	public static PropertyResourceBundle appProp = (PropertyResourceBundle)ResourceBundle.getBundle(propertyFileName);
	
  public static String urlMockaroo = appProp.getString("api.url.mockaroo");	
  public static String keyMockaroo = appProp.getString("api.key.mockaroo");	
}
