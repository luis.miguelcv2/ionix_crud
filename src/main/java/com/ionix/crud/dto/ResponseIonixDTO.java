package com.ionix.crud.dto;

public class ResponseIonixDTO {
  private int responseCode;
  private String description;
  private long elapsedTime;
  private TotalItems result;

  public int getResponseCode() {
    return responseCode;
  }

  public void setResponseCode(int responseCode) {
    this.responseCode = responseCode;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public long getElapsedTime() {
    return elapsedTime;
  }

  public void setElapsedTime(long elapsedTime) {
    this.elapsedTime = elapsedTime;
  }

  public TotalItems getResult() {
    return result;
  }

  public void setResult(TotalItems result) {
    this.result = result;
  }

  public static class TotalItems {
    private int registerCount;

    public int getRegisterCount() {
      return registerCount;
    }

    public void setRegisterCount(int registerCount) {
      this.registerCount = registerCount;
    }
    
  }
}
