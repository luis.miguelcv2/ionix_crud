package com.ionix.crud.dto;

import java.util.List;

public class ResponseMockarooApiDTO {
  private int responseCode;
  private String description;
  private Items result;

  public int getResponseCode() {
    return responseCode;
  }

  public void setResponseCode(int responseCode) {
    this.responseCode = responseCode;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Items getResult() {
    return result;
  }

  public void setResult(Items result) {
    this.result = result;
  }

  public static class Items {
    private List<Item> items;

    public List<Item> getItems() {
      return items;
    }

    public void setItems(List<Item> items) {
      this.items = items;
    }
  }

  public static class Item {
    private String name;
    private DetailsItem detail;

    public String getName() {
      return name;
    }
    public void setName(String name) {
      this.name = name;
    }
    public DetailsItem getDetail() {
      return detail;
    }
    public void setDetail(DetailsItem detail) {
      this.detail = detail;
    }
  }

  public static class DetailsItem {
    private String email;
    private String phoneNumber;

    public String getEmail() {
      return email;
    }
    public void setEmail(String email) {
      this.email = email;
    }
    public String getPhoneNumber() {
      return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
      this.phoneNumber = phoneNumber;
    }
    
  }
}
