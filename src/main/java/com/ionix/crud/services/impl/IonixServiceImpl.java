package com.ionix.crud.services.impl;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.ionix.crud.config.AppProperties;
import com.ionix.crud.services.IonixService;
import com.ionix.crud.utils.https.TrustAllCerts;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

@Service
public class IonixServiceImpl implements IonixService {

  @Override
  public List<Object> mockarooApi(String paramCipher) {
    List<Object> response = new ArrayList<>();

    Request request = new Request.Builder().url(AppProperties.urlMockaroo + "/" + paramCipher)
      .method("GET", null)
      .addHeader("X-API-Key", AppProperties.keyMockaroo)
      .build();      

    try {
      OkHttpClient client = TrustAllCerts.getTrustAllCerts();
      Response responseApi = client.newCall(request).execute();
  
      response.add(responseApi.code());
      response.add(responseApi.body().string());
    } catch (IOException e) {
      e.printStackTrace();	        	
    } catch (KeyManagementException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException e) {
      e.printStackTrace();
    }

    return response;
  }
  
}
