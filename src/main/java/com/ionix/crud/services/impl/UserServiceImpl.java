package com.ionix.crud.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ionix.crud.dao.UserRepository;
import com.ionix.crud.model.User;
import com.ionix.crud.services.UserService;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

  @Override
  public List<User> findUserAll() {
    return userRepository.findAll();
  }

  @Override
  public Optional<User> findUserById(Long id) {
    return userRepository.findById(id);
  }

  @Override
  public User saveUser(User user) {
    return userRepository.save(user); 
  }

  @Override
  public void deleteUser(User user) {
   userRepository.delete(user);
  }

  @Override
  public List<User> findAllByUsername(String username) {
    return userRepository.findByUsername(username);
  }
}
