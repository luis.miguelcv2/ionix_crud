package com.ionix.crud.services;

import java.util.List;
import java.util.Optional;

import com.ionix.crud.model.User;


public interface UserService {
  List<User> findUserAll();
  List<User> findAllByUsername(String username);
  Optional<User> findUserById(Long id);
  User saveUser(User user);
  void deleteUser(User user);
}
