package com.ionix.crud;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

@SpringBootTest
class CrudApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testApiIonix() throws URISyntaxException 	{
			final String baseUrl = "http://localhost:8081/ionix/mockaroo";
			URI uri = new URI(baseUrl);
		
			HttpHeaders headers = new HttpHeaders();
			headers.set("param", "1-9");      

			HttpEntity<String> request = new HttpEntity<>("", headers);
			 
			ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
			
			assertEquals(200, result.getStatusCodeValue());
			//assertEquals(1, 1);
	}

}
