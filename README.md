# ionix_crud
Ionix Crud Users

## Requeriments
  * [Maven](https://maven.apache.org/install.html)
  * Java 1.8
  * Create mysql database "ionixdb" on localhost
    * ```bash 
      CREATE DATABASE ionixdb; 
      ```

## Credentials
  * Database 
    * user: ionix
    * password: lS4C87XfAdUK).iV

  * App Web (Crud)
    * user: ionix
    * password: 123456

## How to start

In order to start the seed use:

```bash
$ git clone https://gitlab.com/luis.miguelcv2/ionix_crud.git
$ cd ionix_crud
$ mvn clean install
$ mvn spring-boot:run
```

## Build an executable JAR
```bash
$ mvn clean package
$ java -jar target/crud-0.0.1.war 
```


Now open your browser at:

  * http://localhost:8081

## Request Mockaroo Api
```bash
curl --request POST \
--url http://localhost:8081/ionix/mockaroo \
--header 'param: 1-9'
```


## Contributors
[<img alt="LuisColmenarez" src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/3625714/avatar.png?width=90" width="117">](https://gitlab.com/luis.miguelcv2)
